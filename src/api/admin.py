from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin
from django.utils.safestring import mark_safe

from api.models import Horchata


class AdminImageMixin:
    @mark_safe
    def admin_image(self, obj):
        return f"<img src='{obj.image.url}' style='height: 100px; width: auto; display: block'/>"

    admin_image.allow_tags = True
    admin_image.short_description = "Preview"


class HorchataAdmin(SortableAdminMixin, AdminImageMixin, admin.ModelAdmin):
    list_display = ("publish", "name", "grade", "date", "admin_image")
    list_display_links = ("name", "date", "admin_image")
    list_editable = ("publish", "grade")


admin.site.register(Horchata, HorchataAdmin)
