import environ
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

# from .models import Horchata

root = environ.Path(__file__) - 2
path = str(root) + "/HorchataClub/.env"
env = environ.Env(
    DEBUG=(bool, False),
)
environ.Env.read_env(path)


@receiver(post_save, sender=User)
def init_new_user(sender, instance, signal, created, **kwargs):
    if created:
        print("Token created")
        Token.objects.create(user=instance)
