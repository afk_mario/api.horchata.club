from django.contrib.auth.models import User
from rest_framework import serializers

from api.models import Horchata


class HorchataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Horchata
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username",)
