# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_auto_20160710_0325'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='horchata',
            name='lat',
        ),
        migrations.RemoveField(
            model_name='horchata',
            name='lon',
        ),
        migrations.RemoveField(
            model_name='horchata',
            name='map_url',
        ),
        migrations.AddField(
            model_name='horchata',
            name='location',
            field=models.CharField(
                verbose_name='Descripcion pequeña', max_length=140,
                default=''),
        ),
    ]
