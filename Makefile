.PHONY: fix format lint

EXECUTABLES = black ruff
K := $(foreach exec,$(EXECUTABLES),\
        $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH")))

format:
	@echo "[Format] src ..."
	@ black --config ./pyproject.toml ./src
	@echo " "

fix:
	@echo "[Fix] running ruff..."
	@ ruff ./src
	@echo " "

run: format fix
	@echo "finished"
