FROM python:3.10-alpine
LABEL maintainer="afk@ellugar.co"

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apk add --no-cache --virtual .build-deps \
        gcc \
        git \
        libwebp-dev \
        jpeg-dev \
        libc-dev \
        linux-headers \
        make \
        musl-dev \
        pcre-dev \
        postgresql-dev \
        zlib-dev 

RUN mkdir /web
WORKDIR /web

COPY ./src/requirements.txt requirements.txt
RUN pip install -r ./requirements.txt

COPY ./src /web

RUN python manage.py collectstatic --noinput

EXPOSE 8000


ENTRYPOINT ["gunicorn", "-b", ":8000", "HorchataClub.wsgi:application"]
